from django import template

register = template.Library()


def resize_to(ingredient, target):
    servings = ingredient.recipe.servings
    if (servings is not None) and (target is not None):
        try:
            unit_amount = ingredient.amount / servings
            return unit_amount * int(target)
        except ValueError:
            pass
    return ingredient.amount


register.filter(resize_to)
