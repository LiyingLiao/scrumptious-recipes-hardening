from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from django.db import IntegrityError

from recipes.forms import (
    RatingForm,
)
from recipes.models import Ingredient, Recipe, ShoppingItem
from django.template.defaulttags import register


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        foods = []
        # for item in self.request.user.shopping_items.all()
        for item in ShoppingItem.objects.filter(user=self.request.user):
            foods.append(item.food)
        # print(foods)
        # print(self.request.user.shopping_items.all())
        context["food_in_shopping_list"] = foods

        if self.request.GET:
            context["input_servings"] = self.request.GET.get("servings")
            # print(servings)
            #  = int(servings)
            # print(context["input_servings"])

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "recipes/list_shopping_items.html"
    paginate_by = 5

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


@login_required
def add_shopping_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(id=ingredient_id)
    user = request.user
    try:
        ShoppingItem.objects.create(
            food=ingredient.food,
            user=user,
        )
    except IntegrityError:
        pass

    return redirect("recipe_detail", pk=ingredient.recipe.id)


@login_required
def delete_shopping_items(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_items_list")


# not be used anyway right now
@register.filter(name="multiply_by_servings")
def multiply_by_servings(value, servings):
    if servings is None:
        return value
    # default_servings = Recipe.servings
    # if default_servings is None:
    #     default_servings = 1
    # return int(value / default_servings * servings)
    return value * servings
